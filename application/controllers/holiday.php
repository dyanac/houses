<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Holiday extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['title'] = "Holiday Houses";
        $this->load->view('holiday.html', $data);
    }

}
