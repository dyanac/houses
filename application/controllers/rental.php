<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rental extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['title'] = "Rental Houses";
        $this->load->view('rental.html', $data);
    }

}
