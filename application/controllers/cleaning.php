<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cleaning extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['title'] = "Home";
        $data['active'] = "Inicio";
        $this->load->view('cleaning.html', $data);
    }

}
