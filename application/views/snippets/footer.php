<div id="footer">
    <div id="footer_container">
        <div id="footer_social">
            <img class="button_social" src="<?php echo base_url();?>images/facebook_32.png" width="100%" height="100%"/>
            <img class="button_social" src="<?php echo base_url();?>images/twitter_32.png" width="100%" height="100%"/>
        </div>
        <div id="footer_info">
            Call Us: 0423950639 <br> Email: edigaro@hotmail.com<br>
            Address: 61 Dundas St. Rye, 3941 Victoria. Australia 
        </div>
    </div>
</div>
</body>
</html>
<script>
    $(document).ready(function() {

        $('#header_parallax').parallax("50%", 0.1);
        $('.bxslider').bxSlider({
            mode: 'fade',
            captions: true,
            auto: true
        });
    });
</script>