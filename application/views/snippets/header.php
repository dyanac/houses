<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 

        <title><?php echo $title; ?></title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css"/>
        <script src="<?php echo base_url(); ?>js/jquery-2.1.1.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Gudea' rel='stylesheet' type='text/css'>
        <!-- slider -->
        <script src="<?php echo base_url(); ?>js/jquery.bxslider.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.bxslider.css" type="text/css"/>
        <!-- parallax -->
        <script src="<?php echo base_url(); ?>js/jquery.localscroll-1.2.7-min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.parallax-1.1.3.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.scrollTo-1.4.2-min.js"></script>
    </head>
    <body>
        <div id="header_container">
            <div id="header_top">
                <div id="header_logo_container">
                    <img id="header_logo" src="<?php echo base_url(); ?>images/logo_2.png" width="100%" height="100%"/>    
                    <div id="header_logo_text">Roberto's Peninsula<br><div style="font-weight: 400;margin-top: -5px; font-size: 15px; color: #fff;">Cleaning Service</div></div>
                </div>
                <div id="header_contact">
                    <img id="header_call" src="<?php echo base_url(); ?>images/call_us.png" width="100%" height="100%"/>
                    <div id="header_us">Call Us:<br><div style="font-weight: bold;">0423950639</div></div>
                </div>
            </div>
            <div id="header_parallax"></div>
        </div>